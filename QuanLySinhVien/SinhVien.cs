﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuanLySinhVien
{
    class SinhVien
    {
        private string hoTen, maSV;
        private double diemToan, diemLy, diemHoa;

        public string HoTen
        {
            get => hoTen;
            set => hoTen = value;
        }

        public string MaSv
        {
            get => maSV;
            set => maSV = value;
        }

        public double DiemToan
        {
            get => diemToan;
            set => diemToan = value;
        }

        public double DiemLy
        {
            get => diemLy;
            set => diemLy = value;
        }

        public double DiemHoa
        {
            get => diemHoa;
            set => diemHoa = value;
        }

        public double DTB(double diemToan, double DiemLy, double DiemHoa)
        {
            return ((diemToan + diemLy + diemHoa) / 3);
        }
        public void XuatTT()
        {
            Console.WriteLine("Ho Ten: {0}|| MSV: {1}|| DiemToan: {2}|| DiemVan|| {3}|| DiemAnh {4}|| DiemTB {5}", hoTen,maSV,diemToan,diemLy, diemHoa, DTB(diemToan, diemLy, diemHoa));
        }
        

        public static void Main(string[] args)

        {
            List<SinhVien> ListSinhVien = new List<SinhVien>();
            int soSV;
            Console.Write("Nhap so SV: ");
            do
            {
                soSV = Convert.ToInt32(Console.ReadLine());
                if (soSV <= 0)
                {
                    Console.WriteLine("Error! So SV phai lon hon 0!");
                    Console.Write("Nhap lai so SV:");
                }
                else break;
            } while (soSV > 0);
           
            
            for (int i = 0; i < soSV; i++)
            {    
                SinhVien sinhVien = new SinhVien();
                Console.Write("Nhap ten: ");
                sinhVien.hoTen = Console.ReadLine();

                Console.Write("Nhap ma sv: ");
                sinhVien.maSV = Console.ReadLine();

                Console.Write("Nhap diem toan: ");
                do
                {
                    sinhVien.diemToan = (double) Convert.ToDouble(Console.ReadLine());
                    if (sinhVien.diemToan > 10 || sinhVien.diemToan < 0)
                    {
                        Console.WriteLine("Error! 0 < Diem Toan < 10");
                        Console.Write("Nhap lai diem toan:");
                    }
                    else break;
                } while (sinhVien.diemToan <= 10 || sinhVien.diemToan >= 0);

                Console.Write("Nhap diem ly: ");
                do
                {
                    sinhVien.diemLy = (double)  Convert.ToDouble(Console.ReadLine());
                    if (sinhVien.diemLy > 10 || sinhVien.diemLy < 0)
                    {
                        Console.WriteLine("Error! 0 < diemLy < 10");
                        Console.Write("Nhap lai diemLy:");
                    }
                    else break;
                } while (sinhVien.diemLy <= 10 || sinhVien.diemLy >= 0);

                Console.Write("Nhap diem hoa: ");
                do
                {
                    sinhVien.diemHoa = (double)  Convert.ToDouble(Console.ReadLine());
                    if (sinhVien.diemHoa > 10 || sinhVien.diemHoa < 0)
                    {
                        Console.WriteLine("Error! 0 < diemHoa < 10");
                        Console.Write("Nhap lai diemHoa:");
                    }
                    else break;
                } while (sinhVien.diemHoa <= 10 || sinhVien.diemHoa >= 0);
                
                ListSinhVien.Add(sinhVien);
            }
            
            for (int i = 0; i < soSV; i++)
            {
                Console.WriteLine("Thong tin sinh vien thu {0}", (i + 1));
                ListSinhVien[i].XuatTT();
                Console.WriteLine("--------------------------------------");
            }

            
                SinhVien sinhVien2 = new SinhVien();
                List<SinhVien> sortSvList = ListSinhVien.OrderByDescending(o => o.DTB(sinhVien2.diemToan, sinhVien2.diemLy, sinhVien2.diemHoa)).ToList();
                Console.WriteLine("3 sinh vien co diem trung binh lon nhat sap xe theo thu tu giam dan :");
           
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("STT {0}||Ten: {1}||Diem TB: {2}",(i+1),sortSvList[i].HoTen, sortSvList[i].DTB(sortSvList[i].DiemToan, sortSvList[i].diemLy, sortSvList[i].diemHoa));
            }
            Console.ReadKey();
            
            
            
        }
    }
}