﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DatabaseFirst_project.Models;
using Microsoft.IdentityModel.Tokens;

namespace DatabaseFirst_project.Controllers
{
    public class HomeController : Controller
    {
        private schoolContext _context = new schoolContext();

        public IActionResult Index()
        {
            return View(_context.Student.ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}