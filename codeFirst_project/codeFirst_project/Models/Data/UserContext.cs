﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;


namespace codeFirst_project.Models.Data
{
    public class UserContext : DbContext
    {
        public UserContext (DbContextOptions<UserContext> options) : base(options)
        {
        
        }
//        public SchoolContext (DbContextOptions<SchoolContext> options)
//            : base(options)
//        {
//        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}