﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace codeFirst_project.Models
{
    [Table("Students")]

    public class User
    {
        [Key]
        private int id;
        
        [Column("Fullname", TypeName="navarchar")]
        [MaxLength(20)]
        private string fullName;
        
        [Column("Address", TypeName="navarchar")]
        [MaxLength(20)]
        private string address;
        
        [Column("Email", TypeName="navarchar")]
        [MaxLength(20)]
        private string email;

        [Column("Phone", TypeName="int")]
        private int phone;

  
        public User()
        {
        }

        public User(int id, string fullName, string address, string email, int phone)
        {
            this.id = id;
            this.fullName = fullName;
            this.address = address;
            this.email = email;
            this.phone = phone;

        }

        public int Id
        {
            get => id;
            set => id = value;
        }

        public string FullName
        {
            get => fullName;
            set => fullName = value;
        }

        public string Address
        {
            get => address;
            set => address = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }
        
        

      
    }
}