﻿namespace codeFirst_project.Models
{
    public class Role
    {
        private int id;
        private string role;

        public Role()
        {
        }

        public Role(int id, string role)
        {
            this.id = id;
            this.role = role;
        }

        public int Id
        {
            get => id;
            set => id = value;
        }

        public string Role1
        {
            get => role;
            set => role = value;
        }
    }
}